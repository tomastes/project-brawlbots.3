-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 22, 2021 at 12:42 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brawlbots-2`
--

-- --------------------------------------------------------

--
-- Table structure for table `contactdata`
--

CREATE TABLE `contactdata` (
  `contact_id` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `phone` int(200) NOT NULL,
  `message` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactdata`
--

INSERT INTO `contactdata` (`contact_id`, `firstname`, `lastname`, `phone`, `message`, `email`) VALUES
(1, 'omer', 'aldiyab', 242423, 'Kiefer ', 'omer@omer.com');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `img_url` varchar(200) NOT NULL,
  `blog_txt` text CHARACTER SET utf8mb4 NOT NULL,
  `date_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL,
  `creator_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `img_url`, `blog_txt`, `date_time`, `creator_id`, `creator_name`) VALUES
(16, 'blog title', 'https://assets.justinmind.com/wp-content/uploads/2018/11/Lorem-Ipsum-alternatives-768x492.png', 'blog text blog text blog text blog text blog text blog text ', '2021-06-18 11:54:24', 27, 'tomas'),
(17, 'second blog', 'https://www.onlineprinters.nl/blog/wp-content/uploads/2019/12/lorem-ipsum.jpg', 'second blog text second blog text second blog text second blog text', '2021-06-18 12:03:17', 27, 'tomas'),
(18, 'blog title', 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg', 'why blog', '2021-06-22 08:34:25', 27, 'tomas'),
(19, 'tom', 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg', 'blog text', '2021-06-22 08:34:42', 27, 'tomas'),
(20, 'my blog', 'https://my-blogger.nl/wp-content/uploads/2019/05/test_ttp_big.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. \r\n', '2021-06-22 09:45:01', 27, 'tomas'),
(21, 'lorem', 'https://placeholder.com/wp-content/uploads/2019/06/lorem-ipsum.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', '2021-06-22 11:49:19', 2, 'qalawi');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `create_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `create_datetime`) VALUES
(27, 'tomas', 'tomastes61@gmail.com', '28b662d883b6d76fd96e4ddc5e9ba780', '2021-06-09 10:21:13'),
(28, 'tomas', 'tomasberhe1999@gmail.com', '28b662d883b6d76fd96e4ddc5e9ba780', '2021-06-09 16:32:09'),
(29, 'tom', 'tomastes61@gmail.', '28b662d883b6d76fd96e4ddc5e9ba780', '2021-06-09 16:38:17'),
(30, 'toas', 'tomastes61@.com', '202cb962ac59075b964b07152d234b70', '2021-06-09 16:39:28'),
(31, 'tomas', 'tomastes61@gmail', 'e4de03e08caf871d20076a9c5b5c273a', '2021-06-10 15:58:20'),
(32, 'omer', 'omer@omer.omer', '916468a2b614352a9b444bd0fbba39f8', '2021-06-11 11:44:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contactdata`
--
ALTER TABLE `contactdata`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contactdata`
--
ALTER TABLE `contactdata`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
