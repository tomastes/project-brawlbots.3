const userInput = document.querySelector(".user_input");
const buttonSubmit = document.querySelector(".button_login");
const toggler = document.querySelector(".toggler");

//!eventListener for buttonToggler
toggler.addEventListener("click", () => {
  //!dislay navigation
  if (document.querySelector(".navigation").style.display == "flex") {
    document.querySelector(".navigation").style.display = "none";
  } else {
    document.querySelector(".navigation").style.display = "flex";
  }
});
