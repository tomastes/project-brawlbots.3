

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    @import url('https://fonts.googleapis.com/css2?family=Oswald:wght@200&display=swap');
 
    .welcome_section{
 background-color: whitesmoke;
padding: 1rem;
/* height: 100vh; */
margin-bottom: 5rem;
    }
    .welcome_section>h2{
        padding: 1rem;
    margin-bottom: 2rem;
    font-family: 'Oswald', sans-serif; 
       font-size: 30px;
    width:50%;
    margin-left: 25%;
    text-align: center;
    box-shadow: 5px 5px 17px 5px rgba(0, 0, 0, 0.34);
    background-image: linear-gradient(to top, #a8edea 0%, #fed6e3 100%);    }
    .welcome_div{
        margin-top: 3rem;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        padding:5px;
        justify-content: space-around;
        box-shadow: 5px 5px 17px 5px rgba(0, 0, 0, 0.34);

    }
    .welcome_div>img,.welcome_div>video{
        width:25rem;
        margin-right: 5px;
        
    }
    .welome_txt{
        display: flex;
        flex-direction: column;
        font-family: 'Oswald',sans-serif;
        width: 60%;
    }
    .welcome_txt_title{
        font-family: 'Oswald', sans-serif; 

    }

    </style>
</head>
<body>
    <?php include_once('./hbs/header.php') ?>
    <?php

?>
<section class="welcome_section">
    <h2>welcome bij 😊brawlbots🤖</h2>
    <div class="welcome_div">
    <img src="http://graphicsdojo.com/wp-content/uploads/2017/04/brawlbot_preview_itch.png" alt="robot image" />
    <div class="welome_txt">
        <h2 class="welcome_txt_title">
        welcome bij 🤖BRAWLBOTS
        </h2>
        <p class="welcome_txt_p">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
         Illum ut, voluptatum sunt dolore 
         adipisci impedit aliquid beatae, non et minima aperiam culpa
          perspiciatis vel pariatur magnam! Magnam culpa necessitatibus illo.
        </p>

    </div>
    
    </div>
    <!--  -->
    <h2>bij  😊brawlbots🤖</h2>
    <div class="welcome_div">
      <div class="welome_txt">
        <h2 class="welcome_txt_title">
        wij zijn 🤖BRAWLBOTS
        </h2>
        <p class="welcome_txt_p">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
         Illum ut, voluptatum sunt dolore 
         adipisci impedit aliquid beatae, non et minima aperiam culpa
          perspiciatis vel pariatur magnam! Magnam culpa necessitatibus illo.
        </p>

    </div>
    <video width="320" height="240" controls>
  <source src="./assets/videos/trailer4.mp4" type="video/mp4">
  <source src="movie.ogg" type="video/ogg">
Your browser does not support the video tag.
</video> 
    
    </div>
    <!--  -->
    <h2>wij  😊zijn brawlbots🤖</h2>
    <div class="welcome_div">
    <img src="https://images.squarespace-cdn.com/content/v1/5b16e15d55b02c10f4a60cce/1578421008977-NYUP0TBZ8873ONTPSFER/ke17ZwdGBToddI8pDm48kNvT88LknE-K9M4pGNO0Iqd7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z5QPOohDIaIeljMHgDF5CVlOqpeNLcJ80NK65_fV7S1UbeDbaZv1s3QfpIA4TYnL5Qao8BosUKjCVjCf8TKewJIH3bqxw7fF48mhrq5Ulr0Hg/vlcsnap-2020-01-07-13h14m29s240.png?format=1500w" alt="robot image" />
    <div class="welome_txt">
        <h2 class="welcome_txt_title">
        wij houden bezijg met  🤖BRAWLBOTS
        </h2>
        <p class="welcome_txt_p">
        Lorem ipsum dolor sit amet consectetur, adipisicing elit.
         Illum ut, voluptatum sunt dolore 
         adipisci impedit aliquid beatae, non et minima aperiam culpa
          perspiciatis vel pariatur magnam! Magnam culpa necessitatibus illo.
        </p>

    </div>
    
    </div>

</section>
   
<?php include_once('./hbs/footer.php') ?>
</body>
<script src='./js/index.js'></script>

</html>
 