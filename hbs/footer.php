<?php
?>
<style> 
/* FOOTER */
/* ----------------------------------------------- */
body{
  
}
footer {
    position: relative;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    background-color: #333;
    padding: 20px 0;
    font-size: 80%;
    display: flex;
    align-self: flex-end;
    flex-direction: column;
    justify-content: center;
}

.links_container {
    display: flex;
    justify-content: space-between;
}

.footer-nav {
    display: flex;
    list-style: none;
    /* margin-left: 4rem; */
}

@media (max-width: 920px) {
    .links_container {
        flex-direction: column;
        justify-content: center;
        align-items: center;
        margin: 0;
        padding: 0;
    }
}

.footer-nav li {
    color: wheat;
    margin: 0 0.5rem;
    font-size: 14px;
    font-family: fantasy;
}

.footer-nav li a {
    color: #888;
    text-decoration: none;
    transition: color 0.2s;
}

.social_links i {
    transition: color 0.4s;
    font-size: 20px;
}

.footer-nav li a:hover {
    color: white;
    transition: color 0.2s;
}

.fa-facebook-f:hover {
    color: #3b5998;
}

.fa-twitter:hover {
    color: #00aced;
}

.fa-instagram:hover {
    color: #dd4b39;
}

.fa-github:hover {
    color: #517fa4;
}

.fa-linkedin:hover {
    color: #517fa4;
}

footer p {
    color: #888;
    text-align: center;
    font-size: 14px;
    font-family: fantasy;
    margin-top: 20px;
}
.github-link{
    color:orange;
    font-size: 18px;
}

/**********END************************footer******************************/

</style>
<footer>
        <div class="links_container">
            <ul class="footer-nav">
            <li><a href="../../brawlbots-3.0/index.php">home</a></li>
          <li><a href="../../brawlbots-3.0/pages/gameStart.php">game</a></li>
          <li><a href="./actors.html">about us</a></li>
          <li><a href="./contact.html">contact</a></li>
          <li><a href="../../brawlbots-3.0/pages/login.php">
        <li> <?php
            if(isset($_SESSION['username'])){
              echo "welcome $_SESSION[username] <br /> <span class='logout_link'> logout </span> ";
            }else{
            echo  'LOGIN';
            }
        ?>
        </li> 
        </a></li>
            </ul>

            <ul class="footer-nav social_links">
                <li>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-instagram"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-linkedin"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-github"></i></a>
                </li>
            </ul>
        </div>

        <p>Copyright &copy; 2021 customized with ❤️ by TMNJ. All rights reserved. <a target="_blank"
                href="https://github.com/tomastes"><i class="fab github-link fa-github"></i></a></p>
    </footer>

    <!-- fontawsome icons -->
    <script src="https://kit.fontawesome.com/b2568cb239.js" crossorigin="anonymous"></script>
