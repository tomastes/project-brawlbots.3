<?php 
session_start();
?>
<style>
@import url('https://fonts.googleapis.com/css2?family=Oswald:wght@200&display=swap');
/******Header and navigation*******/
header {
  display: flex;
  flex-direction: row;
  background-image: linear-gradient(to right, #434343 0%, black 100%);
  justify-content: space-between;
  align-items: center;
 
  overflow: scroll;
 
  z-index: 2;
}

header img {
  max-width: 170px;
  max-height: 80px;
  flex: 0.1;
}
.navigation {
  justify-content: space-around;
  display: flex;
  flex: 0.8;
  padding: 0 1rem;
  margin-right: 3rem;
  width: 100%;

}
.navigation ul {
flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  list-style: none;
  text-decoration: none;
  align-items: center;
}
.navigation ul li {
  font-size: 16px;
  font-family: 'Oswald', sans-serif;  transition: 0.2s ease-in;
  font-weight: 800;
}
.navigation ul li a {
  text-decoration: none;
  color: rgb(230, 230, 230);
}
.navigation ul li a:hover {
  color: rgb(173, 250, 183);
  font-weight: 900;
}

.navigation ul li:hover {
  border-bottom: 1px solid grey;
  transform: scale(1.1);
  transition: 0.2s ease-out;
}
.toggler {
  display: none;
  font-size: 30px;
  margin-right: 2rem;
  color: rgb(1, 68, 37);
}
.toggler a {
  color: rgb(19, 153, 90);
}

/* @media (max-width: 920px) {
  .navigation {
    flex: 0.9;
  }
  .navigation ul li {
    font-size: 15px;
    transition: 0.2s ease-in;
  }
} */
@media (max-width: 920px) {
  .navigation {
    display: none;
  }
  .navigation>ul{
    display: flex;
    flex-direction: column;
    justify-content: space-between !important;

  }
  .toggler {
    display: block;
  }
}
.user_info{
  display: flex;
  flex-direction: column;
}
.logout_link{
  font-size: 11px;
  font-weight: 100;
}
/***********END******Header and navigation*******/
</style>
<header>
      <img
        src="https://www.vhv.rs/dpng/d/468-4688935_brawl-stars-wiki-boss-robot-brawl-stars-hd.png"
        alt=""
      />
      <nav class="navigation">
        <ul class="nav_lists">
          <li><a href="http://localhost:8888/brawlbots-3.0">HOME</a></li>
          <li><a href="../../brawlbots-3.0/pages/gameStart.php">GAME</a></li>
          <li><a href="../../brawlbots-3.0/pages/blog.php">BLOG</a></li>
          <li><a href="../../brawlbots-3.0/pages/developers.php">OVER ONS</a></li>
          <li><a href="../../brawlbots-3.0/pages/contact.php">CONTACT</a></li>
          <li class="user_info">
          <?php 
              if(isset($_SESSION['username']) && isset($_SESSION['id'])){
                echo "<a href=http://localhost:8888/brawlbots-3.0/functions/logout.php> welcome $_SESSION[username] <br /> <span class=logout_link> logout </span> </a>";
              }else{
                echo '<a href="http://localhost:8888/brawlbots-3.0/pages/login.php"> LOGIN </a>';
              }
          ?>
          </li>
         
   
        </ul>
      </nav>
      <div class="toggler">
        <a href="#"><i class="fas fa-hamburger"></i></a>
      </div>
    </header>