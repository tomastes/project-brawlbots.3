
<?php
include_once("../hbs/header.php");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Over ons</title>

  <link rel="stylesheet" href="style.css">
</head>

<body>
<div id="one">
    <div class="container1">
  <div class="row">
  <svg viewBox="0 0 960 300">
    <symbol id="s-text">
    <text text-anchor="middle" x="50%" y="80%">Over BotBrawl </text>
  </symbol>

  <g class = "g-ants">
    <use xlink:href="#s-text" class="text-copy"></use>
    <use xlink:href="#s-text" class="text-copy"></use>
    <use xlink:href="#s-text" class="text-copy"></use>
    <use xlink:href="#s-text" class="text-copy"></use>
    <use xlink:href="#s-text" class="text-copy"></use>
  </g>
</svg>
  </div>
  
    </div>
    
           <!-- CONTENT
=================================-->

        <div class="jumbotron"><span class="fwbg-title"> About Active Swag </span>
            <div class="headline-container">
                <div class="row">
                    <div class="col-xs-12"><span class="fwbg-callout"> </span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /header container-->
<!-- CONTENT
=================================-->
<div class="container" style="max-width: 1200px;">
    <div class="row">
        <div class="col-xs-12" style="text-align:center;">
            <h2>– About Us –</h2></div>
        <div class="col-xs-12 col-sm-8 col-md-6">
            <div class="info-text">
                <p>We know how important it is to make a meaningful connection with your customer or consumer and that the right promotional product is part of the equation. The Swag Team at Active International is your dedicated resource for branded premiums, corporate gifts and promotional items.</p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-6">
            <div class="col-xs-12">
                <figure class="snip1374"><a href="#"><img src="https://images-na.ssl-images-amazon.com/images/I/81hypYRurrL.jpg" alt="sample66" /></a></figure>
            </div>
        </div>
        <div style="clear: both;"></div>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-push-4 col-md-6 col-md-push-6">
                <div class="info-text right">
                    <p>With more than a decade of experience working with media companies, retailers and manufacturers, our goal is to make sure your experience is exceptional, whether we’re recommending new ideas, managing creative or ensuring fulfillment.</p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-6 col-md-pull-6">
                <div class="col-xs-12 col-sm-12">
                    <figure class="snip1374"><a href="#"><img src="https://www.menkind.co.uk/media/catalog/product/cache/image/1000x/beff4985b56e3afdbeabfc89641a4582/h/e/hexbug_battlebots_arena_55371_6_.jpg" alt="sample57" /></a></figure>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <hr>
        <div class="row">
            <div class="col-xs-12" style="text-align:center;padding-top:25px;">
                <h3 style="padding-bottom:10px;text-transform:uppercase;">contact us vandaag for:</h3>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <figure class="snip0078 blue"><img src="https://cdn.shopify.com/s/files/1/2204/2817/files/hottest_trends_2-0078.jpg?1369754927576680618" alt="sampl45" />
                        <figcaption>
                            <h2>Contact <span> Pagina</span></h2>
                        </figcaption>
                        <a href="../pages/contact.php"></a></figure>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <figure class="snip0078 red"><img src="https://cdn.shopify.com/s/files/1/2204/2817/files/budget_friendly_ideas_3-0078.jpg?15005372342585381625" alt="sample50" />
                        <figcaption>
                            <h2>Blog <span>pagina</span></h2>
                        </figcaption>
                        <a href="../pages/blog.php"></a></figure>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <figure class="snip0078 yellow"><img src="http://ehacks.xmlfile.us/wp-content/uploads/2018/10/Badland-Brawl-Hack-Gems-Badland-Brawl-Cheats.jpg" alt="sample46" />
                        <figcaption>
                            <h2>Spel <span> Bagina</span></h2>
                        </figcaption>
                        <a href="../pages/gameStart.php"></a></figure>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



  <div class="container">
    <h1 class="heading"><span>Game</span>Developers</h1>

    <div class="profiles">
      <div class="profile">
        <img src="../assets/images/jarvin.png" class="profile-img">

        <h3 class="user-name">Jarvin</h3>
        <h5>Creative Director</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eum eveniet soluta hic sunt sit reprehenderit.</p>
        <p>mike@example.com</p>
        <p><a href="../pages/contact.php" class="button">Link Button</a></p>
      </div>
      <div class="profile">
        <img src="../assets/images/tomas.png" class="profile-img">

        <h3 class="user-name">Tomas tesinmaream</h3>
        <h5>Managing Partner</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam facilis sint quod.</p>
        <p>mike@example.com</p>
        <p><a href="../pages/contact.php" class="button">Link Button</a></p>
      </div>
      <div class="profile">
        <img src="../assets/images/Mo.jpg" class="profile-img">

        <h3 class="user-name">Mohammad Aldyab</h3>
        <h5>Project Manager</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore, eveniet!</p>
        <p>mike@example.com</p>
        <p><a href="../pages/contact.php" class="button">Link Button</a></p>
      </div>
      <div class="profile">
        <img src="../assets/images/nicholas.png" class="profile-img">

        <h3 class="user-name">Nicholas</h3>
        <h5>Project Manager</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore, eveniet!</p>
        <p>mike@example.com</p>
        <p><a href="../pages/contact.php" class="button">Link Button</a></p>
      </div>
    </div>
  </div>
</body>
<style>
    @import url("https://fonts.googleapis.com/css?family=Poppins&display=swap");

* {
  padding: 0;
  margin: 0;
}

body {
  background-color: #12172b;
  font-family: "Poppins", sans-serif;
  
}

.container {
  margin: 20px 40px;
  color: white;
}

.heading {
  font-size: 60px;
  color: white;
  margin-left:30%;
}

.heading span {
  font-style: italic;
  font-size: 30px;
}

.profiles {
  display: flex;
  justify-content: space-around;
  margin: 20px 80px;
}

.profile {
  flex-basis: 260px;
}

.profile .profile-img {
  height: 150px;
  width: 150px;
  border-radius: 50%;
  filter: grayscale(100%);
  cursor: pointer;
  transition: 400ms;
}

.profile:hover .profile-img {
  filter: grayscale(0);
}

.user-name {
  margin-top: 10px;
  font-size: 25px;
}

.profile h5 {
  font-size: 10px;
  font-weight: 80;
  letter-spacing: 3px;
  color: #ccc;
}

.profile p {
  font-size: 12px;
  margin-top: 20px;
  text-align: justify;
  max-width:170px;
}
.centertext p {
    text-align: center;
    font-size: 20px;
    color: #ff0000;
    font-weight: bold;
    animation: textanimation2 0.5s ease-in-out;
}
h2{
  color:white;
}

@keyframes textanimation2 {
    0% {
        transform: scale(0);
    }
    100% {
        transform: scale(1);
    }
}
.btn :hover {
  color: #fff;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
      }
      .btn a{
        color: #999999;
      }
      video{
        width: 50%;
        margin-left: 30%;
        height: 20%;
        margin-top: 10%;
      }
      #namee{
      box-sizing: border-box;
      background-color: #fc3153;
      font-family: 'Luckiest Guy';
     
      width: 150px;
        height: 50px;
        font-size: 2vw;
        margin: left;
        text-align: center;
        color: white;
     }
     #namee p{
      color: #484848;
  font-size: 910px;
  font-weight: bold;
  font-family: monospace;
  letter-spacing: 7px;
  cursor: pointer
     }
     #namee span{
      transition: .5s linear
}
#namee:hover span:nth-child(1){
  margin-right: 5px
}
#namee:hover span:nth-child(1):after{
  content: "'";
}
#namee:hover span:nth-child(2){
  margin-left: 30px
}
#namee:hover span{
  color: #fff;
  text-shadow: 0 0 10px #fff,
               0 0 20px #fff, 
               0 0 40px #fff;
}
svg {
    display: block;
    font: 10.5em 'Montserrat';
    width: 960px;
    height: 300px;
    margin: 0 auto;
}

.text-copy {
    fill: none;
    stroke: white;
    stroke-dasharray: 6% 29%;
    stroke-width: 5px;
    stroke-dashoffset: 0%;
    animation: stroke-offset 5.5s infinite linear;
}

.text-copy:nth-child(1){
    stroke: #4D163D;
  animation-delay: -1;
}

.text-copy:nth-child(2){
  stroke: #840037;
  animation-delay: -2s;
}

.text-copy:nth-child(3){
  stroke: #BD0034;
  animation-delay: -3s;
}

.text-copy:nth-child(4){
  stroke: #BD0034;
  animation-delay: -4s;
}

.text-copy:nth-child(5){
  stroke: #FDB731;
  animation-delay: -5s;
}

@keyframes stroke-offset{
  100% {stroke-dashoffset: -35%;}
}
.button {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
@import url(https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css);
@import url('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600|Oswald:600,700');
figure.snip0078 {
    font-family: 'Oswald', Arial, sans-serif;
    color: #fff;
    position: relative;
    float: left;
    margin: 10px 1%;
    min-width: 220px;
    max-width: 350px;
    max-height: 310px;
    width: 100%;
    text-align: center;
}

figure.snip0078 * {
    -webkit-box-sizing: padding-box;
    box-sizing: padding-box;
}

figure.snip0078 img {
    opacity: 1;
    max-width: 100%;
    border: 10px solid #000000;
    -webkit-transition: all 0.3s;
    transition: all 0.3s;
    -webkit-transform: scale(0.85);
    transform: scale(0.85);
    -webkit-transform-origin: 0 0;
    transform-origin: 0 0;
}

figure.snip0078 figcaption {
    bottom: 0;
    width: 60%;
    right: 0;
    position: absolute;
    background: #000000;
    padding: 12px;
    -webkit-transition: all 0.3s;
    transition: all 0.3s;
    -webkit-transform: translateY(0);
    transform: translateY(0);
    box-shadow: 0 0px 10px -10px #000000;
}

figure.snip0078 figcaption h2,
figure.snip0078 figcaption p {
    margin: 0;
    color: #ffffff;
}

figure.snip0078 figcaption h2 {
    font-weight: 400;
    text-transform: uppercase;
}

figure.snip0078 figcaption h2 span {
    font-weight: 800;
}

figure.snip0078 figcaption p {
    font-size: 0.9em;
    font-weight: 500;
    opacity: 0.65;
}

figure.snip0078 a {
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    position: absolute;
    z-index: 1;
}

figure.snip0078.blue figcaption {
    background: #315e95;
}

figure.snip0078.blue img {
    border: 10px solid #315e95;
}

figure.snip0078.red figcaption {
    background: #81261d;
}

figure.snip0078.red img {
    border: 10px solid #81261d;
}

figure.snip0078.yellow figcaption {
    background: #a85913;
}

figure.snip0078.yellow img {
    border: 10px solid #a85913;
}

figure.snip0078:hover figcaption,
figure.snip0078.hover figcaption {
    -webkit-transform: translateY(-8px);
    transform: translateY(-8px);
    box-shadow: 0 15px 15px -15px #000000;
}

figure.snip0078:hover.blue img,
figure.snip0078.hover.blue img {
    border: 10px solid #2980b9;
}

figure.snip0078:hover.red img,
figure.snip0078.hover.red img {
    border: 10px solid #c0392b;
}

figure.snip0078:hover.yellow img,
figure.snip0078.hover.yellow img {
    border: 10px solid #e67e22;
}


.snip1374 {
    position: relative;
    float: left;
    overflow: hidden;
    margin: 10px 1%;
    min-width: 240px;
    max-width: 500px;
    width: 100%;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
    border-radius: 50%;
}

.snip1374 img {
    max-width: 100%;
    vertical-align: top;
    -webkit-transition: all 2s ease-out;
    transition: all 2s ease-out;
    -webkit-transform: scale(1.4);
    transform: scale(1.4);
}

.snip1374:hover img,
.snip1374.hover img {
    -webkit-transform: scale(1);
    transform: scale(1);
}
.info-text{
  top: 50%;
}
.info-text p {
    display: table-cell;
    margin: 0;
    vertical-align: middle;
    text-align: left;
    font-size: 26px;
    line-height: 1.4;
    font-family: 'Open Sans', sans-serif;
    padding: 16% 4%;
    vertical-align: middle;
}
.info-text .right {
  float: right;
}
@media only screen and (max-width: 1150px) {
  .profiles {
    flex-direction: column;
  }

  .profile {
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .profile p {
    text-align: center;
    margin: 20px 60px 80px 60px;
    font-size: 20px;
  }
}

@media only screen and (max-width: 900px) {
  .heading {
    font-size: 40px;
    color: white;
    text-align: center;
  }

  .heading span {
    font-size: 15px;
  }

  .profiles {
    margin: 20px 0;
  }

  .profile p {
    margin: 20px 10px 80px 10px;
  }
}

    </style>

</html>

<?php
include_once("../hbs/footer.php");
?>
