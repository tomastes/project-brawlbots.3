<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
.game_form{
    background-color:whitesmoke;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height:100vh;

}
.game_form>form{
padding:2rem;
background-color:white;
width:100%;
display:flex;
justify-content:center;
align-items: center;
flex-direction: column;
}
.input_container{
display: flex;
width:100%;
justify-content: space-between;
align-items: center;
flex-direction: column;
}

.input_container>input{
    padding:1rem .5rem;
    border-top: none !important;
    border-right: none !important;
    border-left: none !important;
    border-bottom: 1px solid gray;
    margin-bottom:1rem;
    outline: none !important;
}
.start_game:hover{
    background-color:gray;
        color:lightblue;
        transition:.3s ease-in-out;
        transform:scale(1.03)
}
.start_game{
    font-size: 22px;
        font-family: fantasy;
        color:gray;
        font-weight: 900;
        text-transform:uppercase;
      padding:1.3rem 4rem;
      background-color: lightblue;
      border: none;
      outline: none;
      cursor: pointer;
      transition:.3s ease-in-out;

      box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px;
}
</style>
</head>

<body>
<?php

include("../hbs/header.php");
?>
<div class="game_form">
<form action="game.php" method="post">

<div class="input_container">
<input required type="text" name="player1" id="player"  placeholder="player1 name"/> <br>
<input required type="text" name="player2" id="player"  placeholder="player2 name"/> <br>
</div>
<input class="start_game" type="submit" value="play" name="game_start" />
</form>

</div>
<script src='../js/index.js'></script>
<?php

include("../hbs/footer.php");
?>
</body>
</html>

