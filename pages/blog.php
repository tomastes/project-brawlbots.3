<?php
include_once("../hbs/header.php");
require_once('../functions/addpost.php');
$posts = getPosts();
// echo count($posts);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
            @import url('https://fonts.googleapis.com/css2?family=Oswald:wght@200&display=swap');

        .section_blog{
           margin-bottom: 17rem;
        }
        
        .section_blog>h2{
            font-family: cursive;
            font-size: 35px;
            margin: auto;
            text-align: center;
            width: 90%;
            font-weight: 900;
            padding: .5rem;
            border-bottom: 1px solid gray;
            color: gray;
            margin-bottom: 1rem;
        }
        .form_container{
            display: flex;
            padding:1rem;
            width:90%;
            align-items: center;
            justify-content: center;
            background-color: whitesmoke;
            margin: auto;
        }
       
        .form_container>form{
            display: flex;
            flex-direction: column;
            width: 70%;
            min-width: 30rem;
        }
        .form_container>form>input{
            padding:1rem .5rem;
            border-top: none ;
             border-right: none ;
             border-left: none ;
             border-bottom: 1px solid gray;
             margin-bottom:1rem;
             outline: none ;
             background: none;
        }
        .form_container>form>button{
        font-size: 22px;
        font-family: fantasy;
        color:gray;
        font-weight: 900;
        align-self: center;
        text-transform:uppercase;
        padding:1.3rem 4rem;
        background-color: lightblue;
        margin-top: 10px;
        width: 50%;
        border: none;
        outline: none;
        cursor: pointer;
        transition:.3s ease-in-out;
        box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px;
        }
        .form_container>form>button:hover{
            background-color:gray;
        color:lightblue;
        transition:.3s ease-in-out;
        transform:scale(1.03)
        }
        .blogs_container{
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
        align-content: center;
        margin-top: 1rem;
        }
 .single_blog{
    
    margin-top: 15px;
    display: flex;
    flex-direction: column;
     width: 20rem;
    
    background: whitesmoke;
    padding: 5px;
    border: 1px solid green;
    box-shadow: rgba(17, 17, 26, 0.05) 0px 4px 16px, rgba(17, 17, 26, 0.05) 0px 8px 32px;
}
.blog_image{
    width: 100%;
    height: 12rem;
   
}
.blog_header{
    display: flex;
    align-items: center;

}
.blog_header > i{
    font-size: 38px;
    color: gray;
}
.blog_header > p{
    margin-left: 5px;
    font-size: 15px;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
}
.blog_icons{
    padding: 5px;

}
.blog_icons > i{
    margin-left: 5px;
    font-size: 20px;
    color:gray;
    cursor: pointer;
    transition: .2s ease-in-out all;

}
.blog_icons > i:hover{
    transform: scale(1.06);
    transition: .2s ease-in-out all;
    color:black;
}
.blog_txt{
    font-size: 15px;
    letter-spacing: .6px;
    text-align: center;
    font-family: cursive;
}
.title{
    font-family: 'Oswald', sans-serif; 

}
.read_more{
    width: 12rem;
    align-self: flex-end;
    padding: 12px 18px;
    border-radius: 40px;
    background: none;
    border: 1px solid orange;
    background-color: whitesmoke;
    font-size: 15px;
    text-transform: uppercase;
    cursor: pointer;

}
    </style>

</head>
<body>
    <section class="section_blog">
        <h2>welcome to our blog page</h2>
    
    <div class="form_container">
    <form action="../functions/addpost.php" method="post">
    <input type="text" required placeholder="add blog title" name="title">
    <input type="text" placeholder="image url" required name="img_url" id="">
    <textarea placeholder="your blog text" name="blog_txt" required id="" cols="30" rows="10"></textarea>
    <button name="submit" type="submit">Add Blog</button>
    </form>
    </div>
    <div class="blogs_container">
        <?php foreach ($posts as $key => $post) : ?>
        <div class="single_blog">
            <div class="blog_header">
            <i class="fas fa-user-circle"></i>
            <p class="blog_name"><?php echo $posts[$key][6]?></p>
            </div>
            <img class="blog_image" src=<?php echo $posts[$key][2]?> alt="">
            <div class="blog_icons">
            <i class="fas fa-thumbs-up"></i>
            <i class="far fa-comment"></i>
            <i class="far fa-paper-plane"></i>
            </div>
            <h2 class="title"><?php echo $posts[$key][1]?></h2>
            <p class="blog_txt"><?php echo $posts[$key][3]?>
                </p>
        <button class="read_more">read more</button>
        </div>
        <?php endforeach; ?>
    </div>

    </section>

<!-- footer -->




<script src='../js/index.js'></script>
</body>
</html>

<?php
include_once("../hbs/footer.php");
?>