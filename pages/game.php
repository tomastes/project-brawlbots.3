<?php

require('../classes/Robot.php');
require('../classes/GameController.php');

session_start();
//make the players multidimensional array
if(isset($_POST['game_start'])){
    $player1 = ['name'=>$_POST['player1'],'hp'=>100,'images'=>['A1','A2','A3','A4','A5']];
    $player2 =['name'=>$_POST['player2'],'hp'=>100,'images'=>['B1','B2','B3','B4','B5']];
    //?multidimensionale array
    $players = [$player1,$player2];
    //session opslaan
    $_SESSION['players']=$players;

    $_SESSION['currPlayer'] = 1;
    echo'<html>
    <body>
    <audio controls autoplay hidden>
   
   <source src="../assets/sounds/mysong.mp3" type="audio/mpeg">
   Your browser does not support the audio element.
 </audio>
    </body>
    </html>';
}

//display the robots

echo "<div class=robots_container >";
for ($i=0; $i <count($_SESSION['players']); $i++) { 
    echo "<div class=single_robot_container id=player$i>";
    //display images based on how much hp
    $robot =  new Robot($_SESSION['players'][$i]['hp'],$_SESSION['players'][$i]['name']);
        $robot->makeRobots($_SESSION['players']);
     if($_SESSION['players'][$i]['hp']<=10){
         echo "<img src=https://media.discordapp.net/attachments/843474465639366657/850334674480005140/GducASs.jpeg />";
    
    }else if($_SESSION['players'][$i]['hp']<=30){
      echo "<img src=https://media.discordapp.net/attachments/843474465639366657/850334679227826226/OBz1Ylr.jpg />";

    }else if($_SESSION['players'][$i]['hp']<=50){
        echo "<img src=https://media.discordapp.net/attachments/843474465639366657/850334673590157332/oDd3sEs.jpeg />";
     
    }else if($_SESSION['players'][$i]['hp']<=70){
  
    echo "<img src=https://media.discordapp.net/attachments/843474465639366657/850334671279751188/kADoP5x.jpeg />";

    } else{
        echo "<img src=https://i.pinimg.com/originals/fb/26/2e/fb262e12a9f4aecb8defc010b195f61f.jpg />";

   
    }
  
    echo "</div>";
    
}
echo "</div>";

//game controller instance
$gameController = new GameController($_SESSION['currPlayer'],$_SESSION['players']);
//check if there is attack
if(isset($_POST['attack'])){
$gameController->attackHandelar();
echo  $gameController->turnHandelar();

}


//function getHp based on Id
 function getHpProcentage($id){
  return  $_SESSION['players'][$id]['hp'];
    
  }
?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
   <style type="text/css">
   .game_container{
    background-color:whitesmoke;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: 100%;
    
   }
    .robots_container{
        background:whitesmoke;
        display:flex;
        flex-direction:row;
        flex-wrap: wrap;
        justify-content: space-evenly;
        align-items: center;
        padding:.5rem;
        box-shadow: rgba(240, 46, 170, 0.4) 5px 5px, rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px, rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
        margin-top: 2rem;
        background-color:lightblue;
    }

    .single_robot_container{
         display: flex;
         align-items: center;
         flex-direction: column;  
         box-shadow: rgba(0, 0, 0, 0.2) 0px 12px 28px 0px, rgba(0, 0, 0, 0.1) 0px 2px 4px 0px, rgba(255, 255, 255, 0.05) 0px 0px 0px 1px inset;

  }
    .single_robot_container>img{
           width:15rem;
          height: 23rem;
    }
    .hp_container{
        width:12rem;
        height:2rem;
        background-color: whitesmoke;
        margin:10px 0;
    }
  #player0 >.hp_container>  .hp_procentage{
        width:<?php echo getHpProcentage(0).'%' ?>;
        background-color:<?php echo getHpProcentage(0)<=20?'#f22c16':'lightgreen';?>;
        height: 2rem;
        margin: 0;
    }
    #player1 >.hp_container>  .hp_procentage{
        width:<?php echo getHpProcentage(1).'%' ?>;
        background-color:<?php echo getHpProcentage(1)<=20?'#f22c16':'lightgreen';?>;
        height: 2rem;
        margin: 0;
    }
    .btn_attack{
        height:3rem; 
    }
    .form_attack{
        display:flex;
        width:100%;
        align-items: center;
        justify-content:center;
    }
    .player_name{
        font-size: 22px;
        font-family: fantasy;
        color:gray;
        font-weight: 900;
        text-transform:uppercase;
        border-bottom: 1px solid gray;

    }
    .button_attack,.button_newgame{
        font-size: 22px;
        font-family: fantasy;
        color:gray;
        font-weight: 900;
        text-transform:uppercase;
      padding:1.3rem 4rem;
      background-color: lightblue;
      border: none;
      outline: none;
      cursor: pointer;
      transition:.3s ease-in-out;

      box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 2px, rgba(0, 0, 0, 0.07) 0px 2px 4px, rgba(0, 0, 0, 0.07) 0px 4px 8px, rgba(0, 0, 0, 0.07) 0px 8px 16px, rgba(0, 0, 0, 0.07) 0px 16px 32px, rgba(0, 0, 0, 0.07) 0px 32px 64px;
    }
    .button_attack:hover,.button_newgame:hover{
        background-color:gray;
        color:lightblue;
        transition:.3s ease-in-out;
        transform:scale(1.03)
    }
    .curr_player{
        display: flex;
        justify-content:center;
        align-items: center;
        width:100%;
        background-color:lightblue;
        margin: 2rem 0;
        box-shadow: rgba(0, 0, 0, 0.2) 0px 12px 28px 0px, rgba(0, 0, 0, 0.1) 0px 2px 4px 0px, rgba(255, 255, 255, 0.05) 0px 0px 0px 1px inset;
    }
    .curr_player>p{
        font-size: 22px;
        font-family: fantasy;
    }
    .winnar_container{
        position: fixed;
        display: flex;
        align-items: center;
        justify-content: center;
        width:100%;
        z-index: 100;
         background-color:    lightpink  ; 
        padding:1rem;
        box-shadow: rgba(0, 0, 0, 0.2) 0px 12px 28px 0px, rgba(0, 0, 0, 0.1) 0px 2px 4px 0px, rgba(255, 255, 255, 0.05) 0px 0px 0px 1px inset;
    }
    .form_newgame{
        background-color: lightseagreen;
        width:80%;
        height:80%;
        display: flex;
        justify-content:center;
        flex-direction: column;
        align-items: center;
    }
    .winnar{
        font-size: 22px;
        font-family: cursive;
        font-weight: 800;
        text-transform: uppercase;
        

    }
   
   </style>
</head>
<body>
<div class="game_container">

<div class="curr_player">
    <p>current player is <?php echo $_SESSION['players'][$_SESSION['currPlayer']]['name']?> </p>
    </div>
   <form class="form_attack" method="post" action="game.php">
    <input <?php echo $gameController->winnar; ?> class="button_attack" type="submit" value="attack" name="attack" />
   </form>
   </div>
   <?php include('../hbs/footer.php') ?>
   <script src='../js/index.js'></script>
 
</body>
</html>